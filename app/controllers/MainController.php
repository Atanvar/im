<?php
namespace app\controllers;

use app\core\FrontController;
use app\models\ProductsModel;
use app\core\Template;

class MainController extends FrontController {

    public function index(){
        $tmp_folder = "index";

        $model = new ProductsModel();
        $article = $model->getArticle(1);
        Template::render($tmp_folder,'main',
            ['title' => $article['title'],
                'text'=>$article['text'],
                'image' => $article['image'],
                'price' => $article['price'],
                'maker' => $article['maker'],
                'title_page' => "Интернет магазин",
            ]);
    }


}