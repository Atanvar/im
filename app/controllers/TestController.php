<?php
/**
 * Created by PhpStorm.
 * User: Atanvar
 * Date: 03.03.2019
 * Time: 1:14
 */

namespace app\controllers;
use app\core\FrontController;

class TestController extends FrontController {

    public function index($id){
        $tmp_folder = "test";
        $this->render($tmp_folder,"test", ["content" => "hello world two"]);
    }

}