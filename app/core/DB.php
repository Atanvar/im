<?php
/**
 * Created by PhpStorm.
 * User: Atanvar
 * Date: 04.03.2019
 * Time: 16:06
 */

namespace app\core;


class DB
{
    static private $db;

    private function dbConnect(){
       if (self::$db === null){
           self::$db = new \PDO('mysql:host=localhost;dbname=im', 'root', '');
           self::$db->exec('SET NAMES UTF8');
       }
        return self::$db;
    }

    static function dbQuery($sql, $param=[]){
        $db = self::dbConnect();

        $query = $db->prepare($sql);
        $query->execute($param);

        return $query;
    }

}