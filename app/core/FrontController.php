<?php

namespace app\core;

use app\core\Router;
use app\core\Template;


class FrontController
{ //todo отрефакторить, выглядит ужаснейше.
    private $controller = "main";
    private $action = "index";
    private $action_param = null;

    /**
     * @return mixed
     */

    static private function getUrl()
    {
        $url = new Router();
        return $url->getUrl();


    }

    static private function explode_ulr()
    {
        $url = self::getUrl();
        $exp_url = explode('/', $url);
        return $exp_url;
    }

    public function getController()
    {
        $array = self::explode_ulr();

        if ($array[1]) {
            $params = explode('?', $array[1]);
            $controller = $params[0];
        } else {
            $controller = $this->controller;

        }
        $controller = ucfirst($controller . "Controller");
        $name_space_path = "\app\controllers\\" . $controller;
        $controller_path = BASE_DIR . '\app\controllers\\' . $controller . ".php";

        if ($array[2]) {
            $params = explode('?', $array[2]);
            $action = lcfirst($params[0]);
//            var_dump($action);exit();
        } else {
            $action = lcfirst($this->action);

        }

        if (!$array[3]) {
            $action_param = $this->action_param;
        } else {
            $action_param = $array[3];
        }

        if (file_exists($controller_path)) {//todo Записывать в логи если нет запрошенного файла
            if (!$action_param) {
                $class = new $name_space_path;
                $class->$action($action_param);
            } else {
                $class = new $name_space_path;
                $class->$action($action_param);
            }


        } else {
            echo "404";
        }

    }
    protected function get($param){
        return $_GET[$param];
    }

}