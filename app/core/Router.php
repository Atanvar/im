<?php
/**
 * Created by PhpStorm.
 * User: Atanvar
 * Date: 28.02.2019
 * Time: 18:12
 */
namespace app\core;
use app\core\FrontController;

class Router
{

    function getUrl(){
         $url = $_SERVER["REQUEST_URI"];
         return $url;
    }

    public static function run(){
        $controller = new FrontController();
        $controller->getController();
    }
}