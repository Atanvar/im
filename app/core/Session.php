<?php
/**
 * Created by PhpStorm.
 * User: Atanvar
 * Date: 05.03.2019
 * Time: 21:47
 */

namespace app\core;


class Session
{
    public static function get($param){
        return $_SESSION[$param];
    }

    public static function put($param, $value){
         $_SESSION[$param] = $value;
    }

}