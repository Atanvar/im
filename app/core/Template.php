<?php
/**
 * Created by PhpStorm.
 * User: Atanvar
 * Date: 03.03.2019
 * Time: 15:48
 */

namespace app\core;


class Template
{
    public static function render($tmp_folder, $view, $params = []){
        extract($params);
        $view = BASE_DIR."\app\\views\\".$tmp_folder.'\\'.$view.".php";

        include $view;
    }

}