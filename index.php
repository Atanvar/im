<?php
session_start();
require_once 'vendor\autoload.php';

use app\core\Router;

define('BASE_DIR', __DIR__);
define('DIR_FO_ASSETS', BASE_DIR."\app\\views\\");
Router::run();
